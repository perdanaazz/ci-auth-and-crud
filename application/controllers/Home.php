<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mahasiswa');
	}
	public function index()
	{
		$queryAllMahasiswa = $this->Mahasiswa->getDataMahasiswa();
		$DATA = array('queryAllMhs' => $queryAllMahasiswa);
		$this->load->view('home', $DATA);
	}
	public function tambah_mahasiswa()
	{
		$this->load->view('tambah_mahasiswa');
	}
	public function edit_mahasiswa($nim)
	{
		$queryMahasiswaDetail = $this->Mahasiswa->getDataMahasiswaDetail($nim);
		$DATA = array('queryMhsDetail' => $queryMahasiswaDetail);
		$this->load->view('edit_mahasiswa', $DATA);
	}
	public function fungsiTambah()
	{
		$nama = $this->input->post('nama');
		$nim = $this->input->post('nim');
		$jurusan = $this->input->post('jurusan');

		$ArrInsert = array(
			'nama' => $nama,
			'nim' => $nim,
			'jurusan' => $jurusan
		);

		$this->Mahasiswa->insertDataMahasiswa($ArrInsert);
		redirect(base_url(''));
	}

	public function fungsiEdit()
	{
		$nama = $this->input->post('nama');
		$nim = $this->input->post('nim');
		$jurusan = $this->input->post('jurusan');

		$ArrUpdate = array(
			'nama' => $nama,
			'nim' => $nim,
			'jurusan' => $jurusan
		);

		$this->Mahasiswa->updateDataMahasiswa($nim, $ArrUpdate);
		redirect(base_url(''));
	}

	public function fungsiDelete($nim){
		$this->Mahasiswa->deleteDataMahasiswa($nim);
		redirect(base_url(''));
	}
}
