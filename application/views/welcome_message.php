<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

	<title>Index</title>
</head>

<body>
	<div class="container">
		<div class="row">
			<h1 class="text-center">Data Mahasiswa</h1>
			<div class="col-4"><a class="btn btn-primary" href="<?php echo base_url('index.php/home/tambah_mahasiswa') ?>">Tambah Data Mahasiswa</a></div>
			<div class="col"></div>
		</div>
		<div class="row">
			<div class="col">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">No</th>
							<th scope="col">Nama</th>
							<th scope="col">NIM</th>
							<th scope="col">Jurusan</th>
							<th scope="col">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
                            $count = 0;
							foreach ($queryAllMhs as $row) {
                                $count = $count + 1;
						?>
						<tr>
							<th><?php echo $count ?></th>
							<td><?php echo $row->nama ?></td>
							<td><?php echo $row->nim ?></td>
							<td><?php echo $row->jurusan ?></td>
							<td><a class="btn btn-warning me-2" href="<?php echo base_url('index.php/welcome/edit_mahasiswa') ?>">Edit</a><a class="btn btn-danger" href="">Hapus</a></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Optional JavaScript; choose one of the two! -->

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	<!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
</body>

</html>